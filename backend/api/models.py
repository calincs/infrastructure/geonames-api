import os
from geoalchemy2 import Geometry, WKTElement, func
from geoalchemy2.comparator import Comparator
from sqlalchemy import and_, text, Index
from sqlalchemy.dialects.postgresql import TSVECTOR

from api import db
from api.utils import dump_geo, dump_results

API_QUERY_LIMIT = os.getenv('API_QUERY_LIMIT', 10)


class Admin1Code(db.Model):
    """ names in English for admin divisions """
    __tablename__ = 'admin1code'
    __table_args__ = (
        db.PrimaryKeyConstraint('code'),
        db.Index('idx_admin1codes_country', 'country_code'),
        db.Index('idx_admin1codes_admin1', 'admin1'),
        db.Index('idx_admin1codes_name', 'name'),
    )
    code = db.Column(db.String(23))
    name = db.Column(db.String(200))
    name_ascii = db.Column(db.String(200))
    geonameid = db.Column(db.Integer)
    country_code = db.Column(db.String(2))
    admin1 = db.Column(db.String(20))

    def __repr__(self):
        return '<Admin1Code %r>' % self.name


class Admin2Code(db.Model):
    """ names for administrative subdivision  """
    __tablename__ = 'admin2code'
    __table_args__ = (
        db.PrimaryKeyConstraint('code'),
        db.Index('idx_admin2codes_admin1', 'admin1'),
        db.Index('idx_admin2codes_admin2', 'admin2'),
        db.Index('idx_admin2codes_name', 'name'),
    )
    code = db.Column(db.String(104))
    country_code = db.Column(db.String(2))
    admin1 = db.Column(db.String(20))
    admin2 = db.Column(db.String(80))
    name = db.Column(db.String(200))
    name_ascii = db.Column(db.String(200))
    geonameid = db.Column(db.Integer)

    def __repr__(self):
        return '<Admin2Code %r>' % self.name


class Geoname(db.Model):
    __tablename__ = 'geoname'
    __table_args__ = (
        db.PrimaryKeyConstraint('geonameid'),
        db.Index('idx_geoname_country', 'country_code'),
        db.Index('idx_geoname_name', 'name'),
        db.Index('idx_geoname_admin1', 'admin1'),
        db.Index('idx_geoname_admin2', 'admin2'),
        db.Index('idx_geoname_fcode', 'feature_code'),
        db.Index('idx_geoname_fclass', 'feature_class'),
        db.Index('idx_geoname_pop', 'population'),
        db.Index('idx_geoname_elev', 'elevation'),
        db.Index('idx_geoname_gtopo', 'gtopo30'),
        db.Index('idx_geoname_tzone', 'timezone'),
        db.Index('idx_geoname_mod', 'moddate'),
        db.Index('idx_geoname_geom', 'the_geom', postgresql_using='gist'),
    )
    geonameid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    asciiname = db.Column(db.String(200))
    alternatenames = db.Column(db.Text)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    feature_class = db.Column(db.String(1))
    feature_code = db.Column(db.String(10))
    country_code = db.Column(db.String(2))
    cc2 = db.Column(db.String(200))
    admin1 = db.Column(db.String(20))
    admin2 = db.Column(db.String(80))
    admin3 = db.Column(db.String(20))
    admin4 = db.Column(db.String(20))
    population = db.Column(db.BigInteger)
    elevation = db.Column(db.Integer)
    gtopo30 = db.Column(db.Integer)
    timezone = db.Column(db.String(40))
    moddate = db.Column(db.Date)
    the_geom = db.Column(Geometry('POINT', 4326, spatial_index=False))
    search_index = db.Column(TSVECTOR)

    def __str__(self):
        return f'{{ "geonameid": {self.geonameid}, "name": "{self.name}", "asciiname": "{self.asciiname}", "alternatenames": "{self.alternatenames}", "latitude": {self.latitude}, "longitude": {self.longitude}, "feature_class": "{self.feature_class}", "feature_code": "{self.feature_code}", "country_code": "{self.country_code}", "cc2": "{self.cc2}", "admin1": "{self.admin1}", "admin2": "{self.admin2}", "admin3": "{self.admin3}", "admin4": "{self.admin4}", "population": {self.population}, "elevation": {self.elevation}, "gtopo30": {self.gtopo30}, "timezone": "{self.timezone}", "moddate": "{self.moddate}", "the_geom": {dump_geo(self.the_geom)}, "search_index": "{self.search_index}" }}'
    
    def __repr__(self):
        return '<Geoname {}>'.format(self.name)

    @staticmethod
    def geocode(iso2=None, name=None, admin1=None, admin2=None, feature_code=None, feature_class=None, id=None, search=None):
        """
        Geocodes a place or city.
        :param iso2: 2 digit country code
        :param name: city or placename
        :param admin1: First level administrative code. Ex., a state in the US. This field is optional.
        :param admin2: Second level administrative code. Ex., a county in the US. This field is optional.
        :param feature_code: Geoname Feature Code
        :param feature_class: Geoname Feature Class
        :param id: GeoNames ID of the place
        :param search: search string for full-text lookups
        :return: a list of geojson results
        """
        
        queries = []
        if search:
            # cleaning search string
            words = search.strip().split()
            query_vector = ' & '.join([f'{word}:*' for word in words])
            queries.append(Geoname.search_index.op(
                '@@')(func.to_tsquery('english', query_vector)))
        if iso2:
            queries.append(Geoname.country_code == iso2.upper())
        if id:
            queries.append(Geoname.geonameid == id)
        if name:
            queries.append(Geoname.name.ilike('%{}%'.format(name)))
        if admin1:
            queries.append(Admin1Code.name.ilike('%{}%'.format(admin1)))
        if admin2:
            queries.append(Admin2Code.name.ilike('%{}%'.format(admin2)))
        if feature_code:
            queries.append(Geoname.feature_code == feature_code.upper())
        if feature_class:
            queries.append(Geoname.feature_class == feature_class.upper())

        results = db.session.query(Geoname.geonameid.label('id'),
                                   Geoname.name.label('name'),
                                   Admin1Code.name.label('admin1'),
                                   Admin2Code.name.label('admin2'),
                                   Geoname.country_code,
                                   Geoname.feature_class,
                                   Geoname.feature_code,
                                   Geoname.population,
                                   Geoname.elevation,
                                   Geoname.gtopo30,
                                   Geoname.timezone,
                                   Geoname.moddate,
                                   Geoname.the_geom) \
            .outerjoin(Admin1Code,
                       and_(Geoname.admin1 == Admin1Code.admin1,
                            Geoname.country_code == Admin1Code.country_code)) \
            .outerjoin(Admin2Code, and_(Geoname.admin2 == Admin2Code.admin2,
                                        Admin1Code.admin1 == Admin2Code.admin1)) \
            .filter(*queries) \
            .group_by(Geoname.geonameid,
                      Geoname.name,
                      Admin1Code.name,
                      Admin2Code.name,
                      Geoname.country_code,
                      Geoname.feature_class,
                      Geoname.feature_code,
                      Geoname.population,
                      Geoname.elevation,
                      Geoname.gtopo30,
                      Geoname.timezone,
                      Geoname.moddate,
                      Geoname.the_geom)
        if search:
            results = results.order_by(func.ts_rank(Geoname.search_index, func.to_tsquery('english', query_vector)).desc())
        results = results.limit(int(API_QUERY_LIMIT))
        # print(results)
        return dump_results(results)

    @staticmethod
    def reverse_geocode(lat=None,
                        lon=None,
                        feature_code=None,
                        feature_class=None,
                        min_population=None,
                        country_code=None):
        """
        Returns nearest city or place based on latitude and longitude input.
        :param lat: latitude
        :param lon: longitude
        :param feature_code: Geoname Feature Code
        :param feature_class: Geoname Feature Class
        :param min_population: minimum population
        :param country_code: iso2 country code
        :return: list of geojson results
        """
        if lon and lat:
            queries = []
            if feature_code:
                queries.append(Geoname.feature_code == feature_code.upper())
            if feature_class:
                queries.append(Geoname.feature_class == feature_class.upper())
            if min_population:
                queries.append(Geoname.population > int(min_population))
            if country_code:
                queries.append(Geoname.country_code == country_code.upper())
            results = db.session.query(Geoname.geonameid.label('id'),
                                       Geoname.name.label('name'),
                                       Admin1Code.name.label('admin1'),
                                       Admin2Code.name.label('admin2'),
                                       Geoname.country_code,
                                       Geoname.feature_class,
                                       Geoname.feature_code,
                                       Geoname.population,
                                       Geoname.elevation,
                                       Geoname.gtopo30,
                                       Geoname.timezone,
                                       Geoname.moddate,
                                       Geoname.the_geom) \
                .outerjoin(Admin1Code,
                           and_(Geoname.admin1 == Admin1Code.admin1,
                                Geoname.country_code == Admin1Code.country_code)) \
                .outerjoin(Admin2Code, and_(Geoname.admin2 == Admin2Code.admin2,
                                            Admin1Code.admin1 == Admin2Code.admin1)) \
                .filter(*queries) \
                .group_by(Geoname.geonameid,
                          Geoname.name,
                          Admin1Code.name,
                          Admin2Code.name,
                          Geoname.country_code,
                          Geoname.feature_class,
                          Geoname.feature_code,
                          Geoname.population,
                          Geoname.elevation,
                          Geoname.gtopo30,
                          Geoname.timezone,
                          Geoname.moddate,
                          Geoname.the_geom).order_by(
                Comparator.distance_centroid(Geoname.the_geom,
                                             func.Geometry(func.ST_GeographyFromText(
                                                 'POINT({} {})'.format(lon, lat)))),
                Geoname.population.desc()).limit(API_QUERY_LIMIT)
            return dump_results(results)
        return {}
