FROM python:3.10-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update
RUN apt-get install -y htop build-essential libpq-dev python3-dev libgeos-dev

WORKDIR /app
ADD ./backend/requirements.txt /app
RUN pip3 install --no-cache-dir -r requirements.txt
ADD ./backend /app

EXPOSE 5000
CMD ["/bin/sh", "-c", "python manage.py run --host 0.0.0.0 --port 5000"]
